import sqlalchemy
from app.models.categories_model import CategoriesModel
from app.models.exceptions import ImportanceUrgencyError
from app.models.tasks_model import TasksModel
from flask import current_app, request, jsonify
from werkzeug import exceptions



def create_task():
    try:
        data = request.get_json()

        values_data = data.pop('categories')
        task = TasksModel(**data)
        task.switch_case()
        eisenhower_classification = task.switch_case()

        for item in values_data:
            for value in item.values():
                category = CategoriesModel.query.filter_by(
                    name=value.title()).first()
                if category == None:
                    new_category = CategoriesModel(**item)
                    current_app.db.session.add(new_category)
                    task.categories.append(new_category)
                else:
                    task.categories.append(category)

        print(task)
        current_app.db.session.add(task)
        current_app.db.session.commit()

        return jsonify({
            "id": task.id,
            "name": task.name,
            "description": task.description,
            "duration": task.duration,
            "eisenhower_classification": eisenhower_classification,
            "categories": task.categories
        }), 201
    except ImportanceUrgencyError as e:
        return e.message, 400
    except sqlalchemy.exc.IntegrityError as e:
        return {"Msg": "Task already exists."}, 409
    
    
def update_task(task_id):
    try:
        data = request.get_json()
        
        task = TasksModel.query.filter_by(
                id=task_id).first_or_404(description="Task not found")
         
        for key, value in data.items():
                setattr(task, key, value)
                task.switch_case()
                eisenhower_classification = task.switch_case()
                
                
        current_app.db.session.add(task)
        current_app.db.session.commit()
        
        return jsonify({
            "id": task.id,
            "name": task.name,
            "description": task.description,
            "duration": task.duration,
            "eisenhower_classification": eisenhower_classification,
        }), 201
    except ImportanceUrgencyError as e:
        return e.message, 400
    except exceptions.NotFound as e:
        return {"Msg": f'{e.description}'}, 404

def delete_task(task_id):
    try:
        task = TasksModel.query.filter_by(
                id=task_id).first_or_404(description="Task not found")
        
        current_app.db.session.delete(task)
        current_app.db.session.commit()

        return '', 204
    except exceptions.NotFound as e:
        return {"Msg": f'{e.description}'}, 404