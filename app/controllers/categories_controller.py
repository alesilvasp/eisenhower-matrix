import sqlalchemy
from app.models.categories_model import CategoriesModel
from flask import current_app, request, jsonify
from werkzeug import exceptions

def create_category():
    try:
        session = current_app.db.session
        
        data = request.get_json()
        
        category = CategoriesModel(**data)
        
        session.add(category)
        session.commit()
        
        return jsonify(category), 201
    except sqlalchemy.exc.IntegrityError as e:
        return {"Msg": "Category already exists."}, 409
    
def update_category(category_id):
    try:
        data = request.get_json()
        
        category = CategoriesModel.query.filter_by(
                id=category_id).first_or_404(description="Category not found")
        
        for key, value in data.items():
                setattr(category, key, value)
                
        current_app.db.session.add(category)
        current_app.db.session.commit()
        
        return jsonify(category), 200
    except exceptions.NotFound as e:
        return {"Msg": f'{e.description}'}, 404
    
    
def delete_category(category_id):
    try:
        category = CategoriesModel.query.filter_by(
                id=category_id).first_or_404(description="Category not found")
        
        current_app.db.session.delete(category)
        current_app.db.session.commit()

        return '', 204
    except exceptions.NotFound as e:
        return {"Msg": f'{e.description}'}, 404
    
def read_all_categories():
    categories = CategoriesModel.query.all()
    
    return jsonify(categories), 200