class ImportanceUrgencyError(Exception):

    def __init__(self, importance, urgency) -> None:

        self.message = {
            "valid_options": {"importance": [1, 2], "urgency": [1, 2]},
            "recived_options": {"importance": importance, "urgency": urgency}
        }

        super().__init__(self.message)
