from sqlalchemy.sql.schema import ForeignKey
from app.configs.database import db
from sqlalchemy import Column, String, Integer, VARCHAR
from dataclasses import dataclass
from .exceptions import ImportanceUrgencyError


@dataclass
class TasksModel(db.Model):
    id: int
    name: str
    description: str
    duration: int
    importance: int
    urgency: int
    eisenhower_id: int

    __tablename__ = "tasks"

    id = Column(Integer, primary_key=True)
    name = Column(VARCHAR(100), unique=True, nullable=False)
    description = Column(String)
    duration = Column(Integer)
    importance = Column(Integer)
    urgency = Column(Integer)
    eisenhower_id = Column(Integer, ForeignKey('eisenhowers.id'))

    def switch_case(self):
        if self.importance == 1 and self.urgency == 1:
            self.eisenhower_id = 1
            return 'Do It First'
        elif self.importance == 1 and self.urgency == 2:
            self.eisenhower_id = 3
            return 'Schedule It'
        elif self.importance == 2 and self.urgency == 1:
            self.eisenhower_id = 2
            return 'Delegate It'
        elif self.importance == 2 and self.urgency == 2:
            self.eisenhower_id = 4
            return 'Delete It'
        else:
            raise ImportanceUrgencyError(self.importance, self.urgency)
