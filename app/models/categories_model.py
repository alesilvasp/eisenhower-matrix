from app.configs.database import db
from sqlalchemy import Column, Integer, VARCHAR, TEXT
from sqlalchemy.orm import relationship, validates
from dataclasses import dataclass
from app.models.tasks_categories_table import tasks_categories


@dataclass
class CategoriesModel(db.Model):
    id: int
    name: str
    description: str
    tasks: list

    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    name = Column(VARCHAR(100), nullable=False, unique=True)
    description = Column(TEXT)
    tasks = relationship(
        'TasksModel', secondary=tasks_categories, backref='categories')

    @validates('name')
    def validate_name(self, key, name):
        return name.title()
