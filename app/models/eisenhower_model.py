from app.configs.database import db
from sqlalchemy import Column, Integer, VARCHAR
from dataclasses import dataclass


@dataclass
class EisenhowerModel(db.Model):
    __tablename__ = 'eisenhowers'

    id = Column(Integer, primary_key=True)
    type = Column(VARCHAR(100))
