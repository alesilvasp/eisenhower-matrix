from flask import Flask


def init_app(app: Flask) -> None:
    from .tasks_blueprint import bp as bp_tasks
    from .category_blueprint import bp as bp_category

    app.register_blueprint(bp_tasks)
    app.register_blueprint(bp_category)
