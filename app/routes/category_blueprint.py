from flask import Blueprint

from app.controllers.categories_controller import create_category, delete_category, update_category, read_all_categories

bp = Blueprint("bp_category", __name__, url_prefix='/category')

bp.post('')(create_category)
bp.patch('/<int:category_id>')(update_category)
bp.delete('/<int:category_id>')(delete_category)
bp.get('')(read_all_categories)
